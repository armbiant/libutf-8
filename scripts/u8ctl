#!/bin/sh
COMMAND=
RUNDIR=
JOBID=

usage() {
    echo "u8ctl *jobid* *cmd=list* args..."
    echo "u8ctl *cmd* *jobid* args..."
}
getabspath ( ) {
    local path=$1
    case ${path} in
	/*) echo ${path}; ;;
	./*) echo $(pwd)${path#./}; ;;
	../*) echo $(dirname $(pwd))${path#../}; ;;
	*)  echo $(pwd)/${path}; ;;
    esac;
}

commandp ( ) {
    local arg=$1;
    case ${arg} in
        kill|info|status)
            return 0;
            ;;
        .*)
            return 0;
            ;;
        *)
            if which "u8ctl_${arg}" 2> /dev/null 1> /dev/null; then
                return 0;
            else
                return 1;
            fi;
            ;;
    esac;
}

find_rundir ( ) {
    local jobid=$1
    local suffix=${2}
    for dir in "." "./_" "~/.u8run" "${U8_RUNDIR}" "${U8RUNDIR}" ${U8RUNDIRS}; do
        if [ -n "$DEBUG" ]; then
            echo "find_rundir check '${dir}' ${jobid} ${suffix} found" "${dir}/${jobid}.${suffix}" >&2;
        fi;
        if [ -z "${dir}" ]; then
            :;
        elif [ -f "${dir}/${jobid}.${suffix}" ]; then
            if [ -n "$DEBUG" ]; then
                echo "found_rundir check '${dir}' ${jobid} ${suffix} found" "${dir}/${jobid}.${suffix}" >&2;
            fi;
            echo "${dir}";
            return 0;
        fi;
    done;
    return 1;
}

check_rundir ( ) {
    local jobid=$1
    local suffix=${2}
    for probe in "." "./" "~/.u8run" "${U8_RUNDIR}" "${U8RUNDIR}" ${U8RUNDIRS}; do
        if [ -f "${probe}/${jobid}.${suffix}" ]; then return 0; fi; done;
    return 1;
}

live_pidp() {
    if kill -0 $1 2>1 > /dev/null; then
	return 0;
    else return 1;
    fi;
}

known_jobid ( ) {
    local jobid=$1;
    if check_rundir ${jobid} ppid; then
        return 0;
    elif check_rundir ${jobid} pid; then
        return 0;
    elif check_rundir ${jobid} log; then
        return 0;
    else
        return 1;
    fi;
}

show_procinfo() {
    pid=$1
    cmd=$2
    if [ -d /proc/${pid} ]; then
	if [ "${cmd}" = "head" ]; then
	    head /proc/${pid}/$3;
	elif [ "${cmd}" = "more" ]; then
	    more /proc/${pid}/$3;
	elif [ -d /proc/${pid}/${cmd} ]; then
	    ls -l /proc/${pid}/${cmd};
	elif [ -L /proc/${pid}/${cmd} ]; then
	    ls -ld /proc/${pid}/${cmd};
	elif [ -f /proc/${pid}/${cmd} ]; then
	    cat /proc/${pid}/${cmd};
	else case ${cmd} in 
		 stdio|logs)
		     ls -l /proc/${pid}/fd/0 /proc/${pid}/fd/1 /proc/${pid}/fd/2;
		     ;;
	     esac;
	fi;
    else echo "Can't get procinfo for pid ${pid}";
    fi;
}
show_ctl_header() {
    local prefix=$1
    if [ -f "${prefix}.pid" ]; then
	fileinfo=$(ls -l "${prefix}.pid");
	echo "("$(cat ${prefix}.pid)") " ${fileinfo};
	( cd $(dirname ${prefix}); ls $(basename ${prefix}).* );
    else
	ls ${prefix}.*;
    fi;
}

# handle_arg ( ) {
#     local arg=$1;
#     if commandp "${arg}"; then
#         COMMAND=$1;
#         return 0;
#     elif [ "${arg%%/*}" != "${arg}" ]; then
#         RUNDIR=${arg%/*};
#         JOBID=${arg##*/};
#         if [ ! -d "${RUNDIR}" ]; then
#             echo "Bad jobid rundir $1";
#             exit;
#         fi;
#         return 0;
#     elif known_jobid ${arg}; then
#         JOBID=${arg};
#         return 0;
#     elif [ "${arg#@}" != "${arg}" ]; then
#         arg="${arg#0}";
#    else
#         return 1;
#     fi;
# }

if commandp $1; then
    COMMAND=$1;
    shift;
else
    echo "Error: bad u8ctl command";
    exit 1;
fi;

if [ -n "$1" ] && known_jobid "$1"; then
    JOBID=$1;
#   echo "Got jobid ${JOBID}";
    shift;
fi;

if [ -z "${RUNDIR}" ]; then
    RUNDIR=$(find_rundir "${JOBID}" ppid || find_rundir "${JOBID}" pid || find_rundir "${JOBID}" log);
#   echo "Got RUNDIR=${RUNDIR}";
fi;

CTL_PREFIX="${RUNDIR}/${JOBID}";

if [ -n "${DEBUG}" ]; then
    echo "u8ctl: jobid=${JOBID} cmd=${COMMAND} rundir=${RUNDIR} ctl_prefix=${CTL_PREFIX}";
fi;

export jobppid jobpid;
if [ -f "${CTL_PREFIX}.ppid" ]; then
    jobppid=$(cat ${CTL_PREFIX}.ppid);
    if ! live_pidp ${jobppid}; then
        echo "Missing manager process (${jobppid}) for ${CTL_PREFIX}.ppid";
        rm -f ${CTL_PREFIX}.ppid;
        jobppid=;
    fi;
fi;
if [ -f "${CTL_PREFIX}.pid" ]; then
    jobpid=$(cat ${CTL_PREFIX}.pid);
    if ! live_pidp ${jobpid}; then
        echo "Missing process (${jobpid}) for ${CTL_PREFIX}.pid";
        rm -f ${CTL_PREFIX}.pid;
        jobppid=;
    fi;
fi;

if [ -f "${CTL_PREFIX}.${COMMAND}" ]; then
    cat ${CTL_PREFIX}.${COMMAND};
    echo;
else case ${COMMAND} in
	 stop|kill)
             if [ -n "${jobppid}" ]; then
                 kill ${jobppid};
             elif [ -n "${jobpid}" ]; then
                 kill ${jobpid};
             else
                 echo "No process information for ${jobid} in ${rundir}";
                 exit 1;
             fi;
	     ;;
	 kill9|crush)
             if [ -n "${jobppid}" ]; then
                 kill -9 ${jobppid};
             elif [ -n "${jobpid}" ]; then
                 kill -9 ${jobpid};
             else
                 echo "No process information for ${jobid} in ${rundir}";
                 exit 1;
             fi;
	     ;;
	 proc|info)
             if [ -n "${jobpid}" ]; then
		 show_procinfo ${jobpid} $1;
	     else
                 echo "No process information for ${jobid} in ${rundir}";
                 exit 1;
	     fi;
	     ;;
	 restart)
	     if [ -f ${CTL_PREFIX}.ppid ] && [ -f ${CTL_PREFIX}.pid ]; then
		 kill $(cat ${CTL_PREFIX}.pid);
	     else
		 echo "Can't restart job for ${jobspec}";
		 exit;
	     fi;
	     ;;
	 pause|continue|resume|hup)
	     if [ -f ${CTL_PREFIX}.ppid ]; then
		 controller = $(cat ${CTL_PREFIX}.ppid);
		 case ${COMMAND} in
		     pause|suspend)
			 kill -STOP ${controller}; break;
			 ;;
		     resume|continue)
			 kill -CONT ${controller}; break;
			 ;;
		     hup)
			 kill -HUP ${controller}; break;
			 ;;
		 esac;
		 exit;
	     else
		 echo "No controller PID found for ${jobspec}";
		 exit;
	     fi;
	     ;;
	 log|tail|logtail)
	     if [ ! -f ${CTL_PREFIX}.log ]; then
		 echo "The log for '${jobspec}' is not available";
	     elif [ $# -gt 2 ]; then
		 tail -$3 ${CTL_PREFIX}.log;
	     else 
		 tail ${CTL_PREFIX}.log;
	     fi;
	     ;;
	 watch|monitor|tailf)
	     if [ ! -f ${CTL_PREFIX}.log ]; then
		 echo "The log for '${jobspec}' is not available";
	     elif [ $# -gt 2 ]; then
		 tail -f -$3 ${CTL_PREFIX}.log;
	     else 
		 tail -f ${CTL_PREFIX}.log;
	     fi;
	     ;;
	 logfile)
	     if [ -L ${CTL_PREFIX}.log ]; then
		 linktext=$(readlink ${ctl_path}.log);
		 echo $(getabspath ${linktext});
	     elif [ -f ${CTL_PREFIX}.log ]; then
		 echo $(getabspath ${ctl_path}.log);
	     else echo "The command for '${jobspec}' is not available"; fi;
	     ;;
	 htop|top)
	     if which htop 2> /dev/null 1> /dev/null; then
		 exec htop -p $(cat ${CTL_PREFIX}.pid);
	     elif which top 2> /dev/null 1> /dev/null; then
		 exec htop -p $(cat ${CTL_PREFIX}.pid);
	     fi;
	     ;;
	 perftop)
	     if which perf 2> /dev/null 1> /dev/null; then
		 exec perf top -p $(cat ${CTL_PREFIX}.pid);
	     else
		 echo "No perf command in path";
	     fi;
	     ;;
	 perf)
	     if which perf 2> /dev/null 1> /dev/null; then
		 if [ $# -eq 0 ]; then
		     exec perf top $* -p $(cat ${CTL_PREFIX}.pid);
		 else
		     perf_cmd=$1; shift;
		     exec perf ${perf_cmd} -p $(cat ${CTL_PREFIX}.pid) $*;
		 fi;
	     else
		 echo "No perf command in path";
	     fi;
	     ;;
	 gdb)
	     if which gdb 2> /dev/null 1> /dev/null; then
		 exec gdb -p $(cat ${CTL_PREFIX}.pid) $*;
	     else
		 echo "No gdb command in path";
	     fi;
	     ;;
	 knogdb)
	     if which knogdb 2> /dev/null 1> /dev/null; then
		 exec knogdb -p $(cat ${CTL_PREFIX}.pid) $*;
	     else
		 echo "No gdb command in path";
	     fi;
	     ;;
	 command|cmd)
	     if [ -f ${CTL_PREFIX}.cmd ]; then
		 cat ${CTL_PREFIX}.cmd;
	     else echo "The command for '${jobspec}' is not available";
	     fi;
	     ;;
	 *)
	     if which "u8ctl_{cmd}" 1> /dev/null 2> /dev/null; then
		 if [ -z "${CTL_PREFIX}" ]; then
		     exec u8ctl_{cmd} "-"  $*;
		 else exec u8ctl_{cmd} "${CTL_PREFIX}"  $*;
		 fi;
	     else
		 echo "The command '${COMMAND}' wasn't recognized";
	     fi;
	     ;;
     esac
fi;
